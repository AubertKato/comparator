package model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;

import model.chemicals.CompFakeTemp;
import model.chemicals.DegradableInputTemplate;
import model.chemicals.InvalidConcentrationException;
import model.chemicals.SequenceVertex;
import model.chemicals.Template;
import model.input.AbstractInput;

public class ComparatorOligoSystem extends OligoSystem<String> {

	public ComparatorOligoSystem(ComparatorGraph<SequenceVertex, String> graph) {
		super(graph);
		graph.exoUseCustomKm = true; 
		Map<String,Double> kms = graph.getCustomExoKm();
		//for all pair of species in a given comparator, we create a CompFakeTemp
		for(ArrayList<SequenceVertex> comp : graph.comparators){
			for(int i = 0; i<comp.size()-1; i++){
				SequenceVertex si = comp.get(i);
				kms.put(""+si, Constants.exoKmInhib); //we are long, so we saturate more.
				//Replace the templates that use it as an input with custom templates.
				//Would be a lot more efficient to look instead for the list of templates starting
				//from the species, but anyway.
				for(String key : this.templates.keySet()){
					Template<String> t = this.templates.get(key);
					if(t.getFrom().equals(si)&&!DegradableInputTemplate.class.isAssignableFrom(t.getClass())){ //might be the other way around
						Template<String> newT = new DegradableInputTemplate<String>(t);
						templates.put(key, newT);
					}
				}
				for(int j = i+1; j<comp.size(); j++){
					
					SequenceVertex sj = comp.get(j);
					this.templates.put("C"+si+"+"+sj, new CompFakeTemp<String>(graph, si, sj));
					//graph.subspecies.put("C"+si+"+"+sj, new String[]{"all"});
					//kms.put("C"+si+"+"+sj+" all", Constants.exoKmTemplate); // TODO: I have to do this later
				}
			}
			SequenceVertex si = comp.get(comp.size()-1); //don't forget the last one
			kms.put(""+si, Constants.exoKmInhib);
			for(String key : this.templates.keySet()){
				Template<String> t = this.templates.get(key);
				if(t.getFrom().equals(si)&&!DegradableInputTemplate.class.isAssignableFrom(t.getClass())){
					Template<String> newT = new DegradableInputTemplate<String>(t);
					templates.put(key, newT);
				}
			}
		}
	}
	
	@Override
	public double getTotalCurrentFlux(SequenceVertex s) {
		double flux = super.getTotalCurrentFlux(s);
		if(((ComparatorGraph<SequenceVertex,String>) graph).hasComparator(s)){
			for(SequenceVertex v : ((ComparatorGraph<SequenceVertex,String>) graph).getComparator(s)){
				if(v.ID!=s.ID){
					Template<String> temp = templates.get("C"+v+"+"+s);
					if(temp == null){
						temp = templates.get("C"+s+"+"+v);
					}
					flux += temp.inputSequenceFlux();
				}
			}
		}
		return flux;
	}
	
	
	@Override
	protected double computeExoKm(double myKm){
		double value = myKm;
		if(graph.exoUseCustomKm){
			Map<String,Double> kms = graph.getCustomExoKm();
			for(String s : kms.keySet()){
				if(s.contains("->")||s.contains("+")){
					//this is a template partial species
					//First, find the edge name. There should be a space between the name and the subspecies
					String edgeName = s.split(" ")[0];
					
					String sub = s.split(" ")[1];
					int subspeciesIndex = Arrays.asList(graph.subspecies.get(edgeName)).indexOf(sub);
					Template<String> temp = this.templates.get(edgeName);
					if(!kms.get(s).equals(-1.0))
						value += myKm*temp.getStates()[subspeciesIndex]/kms.get(s);
				}else{
					//This is a signal species
					SequenceVertex seq = graph.getEquivalentVertex(new SequenceVertex(Integer.parseInt(s.substring(1))));
					value += seq.getConcentration()*myKm/kms.get(s);
				}
			}
		} else {
		for (SequenceVertex s: this.sequences) {
			double tempKm = (graph.inhibitors.containsKey(s)?Constants.exoKmInhib:Constants.exoKmSimple);
			
			value += s.getConcentration()*(tempKm==myKm?1.0:myKm/tempKm);
		}
		if(graph.exoSaturationByFreeTemplates){
			for(Template<String> t: this.templates.values()){
				value += (myKm*t.concentrationAlone)/t.exoKm;
			}
		}
		}
		return value;
	}
	
	@Override
	public void computeDerivatives(double t, double[] y, double[] ydot) {
		// First all the activation sequences, then inibiting, then templates.
		// ydot is a placeholder, should be updated with the derivative of y at
		// time t.
		
		
		
		int where = 0;
		boolean saveActivity = (t >= this.time +1);
		Iterator<SequenceVertex> it = this.sequences.iterator();
		while(it.hasNext()){
			
						it.next().setConcentration(y[where]);
			
						where++;
			}

		double[] internal;
		Iterator<Template<String>> it2 = this.templates.values().iterator();
		while(it2.hasNext()) {
			Template<String> templ = it2.next();
			int length = templ.getStates().length;
			internal = new double[length];
			for(int i = 0; i< length; i++){
			internal[i] = y[where]; // there must be a better way. There is with arraycopy...
			where++;
			}
			try {
				templ.setStates(internal);
			} catch (InvalidConcentrationException e) {
				
				e.printStackTrace();
				//System.exit(-1);
			}
		}
		
		
		for(Template<String> temp : this.templates.values()){
			if(DegradableInputTemplate.class.isAssignableFrom(temp.getClass())){ //might be the other way around
				((DegradableInputTemplate) temp).setExo(Constants.exoVm/computeExoKm(temp.exoKm));
			}
		}
		
		if (graph.saturablePoly){
			this.setObservedPolyKm();
		}
		if (graph.saturableNick){
			this.setObservedNickKm();
		}
		
		if (saveActivity){
			//System.out.println("Saving stuff "+this.time);
			this.time++;
			//System.out.println("New pol activity: "+time+" "+this.templates.values().iterator().next().poly+" values:"+y[0]+" "+y[1]);
			this.savedActivity[0][time] = graph.saturableExo?Constants.exoKmSimple/ this.computeExoKm(Constants.exoKmSimple):1;
			//ret[where] = graph.saturablePoly?this.templates.values().iterator().next().poly:1;
			this.savedActivity[1][time] = graph.saturablePoly?this.templates.values().iterator().next().getCurrentPoly()/(Constants.polVm/Constants.polKm):1;
			//ret[where] = graph.saturablePoly?this.templates.values().iterator().next().nick:1;
			this.savedActivity[2][time] = graph.saturablePoly?this.templates.values().iterator().next().getCurrentNick()/(Constants.nickVm/Constants.nickKm):1;
		}
		
		where = 0;
		it = this.sequences.iterator();
		SequenceVertex seq;
		while(it.hasNext()){
					seq = it.next();
					ydot[where] = this.getTotalCurrentFlux(seq);
					for(AbstractInput inp : seq.inputs){
						ydot[where]+=inp.f(t);
					}
					where++;
				}

		it2 = this.templates.values().iterator();
		while(it2.hasNext()) {
			internal = it2.next().flux();
			for(int i=0; i<internal.length;i++){
			ydot[where] = internal[i];
			where++;
			}
			
		}
		//if(t>=10)
		//System.out.println("Alternative test: time "+t+" [y] "+y[0]+" d "+ydot[0]);
		where++;
	}

}
