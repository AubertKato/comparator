package graphical.frame;

import edu.uci.ics.jung.algorithms.layout.FRLayout;
import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.visualization.decorators.ToStringLabeller;
import graphical.graphRendering.ComparatorOligoRenderer;
import graphical.graphRendering.MyVertexFillPaintTransformer;
import graphical.graphRendering.OligoRenderer;

import java.awt.Dimension;

import javax.swing.border.LineBorder;

import utils.EdgeFactory;
import utils.VertexFactory;
import model.ComparatorGraph;
import model.OligoGraph;
import model.chemicals.SequenceVertex;

public class ComparatorMain extends MainFrame {
	
	@Override
	protected void initGraph(){
		final ComparatorGraph<SequenceVertex, String> g = new ComparatorGraph<SequenceVertex,String>();
	    g.initFactories(new VertexFactory<SequenceVertex>(g){
	    	
			public SequenceVertex create() {
				SequenceVertex newvertex = associatedGraph.popAvailableVertex();
				if (newvertex == null){
					newvertex = new SequenceVertex(associatedGraph.getVertexCount() + 1);
				} else {
					newvertex = new SequenceVertex(newvertex.ID);
				}
				return newvertex;
			}

			@Override
			public SequenceVertex copy(SequenceVertex original) {
				 SequenceVertex ret = new SequenceVertex(original.ID);
				 ret.inputs = original.inputs;
				 return ret;
			} 	
	    }, new EdgeFactory<SequenceVertex,String>(g){
	    	public String createEdge(SequenceVertex v1, SequenceVertex v2){
	    		return v1.ID+"->"+v2.ID;
	    	}
	    	public String inhibitorName(String s){
	    		return "Inhib"+s;
	    	}
	    });
	    this.graph = g;
	}

	@Override
	protected MyVisualizationServer<SequenceVertex,String> initVisualizationServer(DataPanel dataPanel){
GraphicInterfaceMouseAdapter myadapter = new GraphicInterfaceMouseAdapter();
        
        Layout<SequenceVertex, String> frlayout = new FRLayout(graph);
        frlayout.setSize(new Dimension(300,300));
		ComparatorVisualizationServer<SequenceVertex,String> vv = new ComparatorVisualizationServer<SequenceVertex,String>(frlayout, myadapter,dataPanel);
        vv.setRenderer(new ComparatorOligoRenderer<SequenceVertex,String>());
        vv.setBorder(LineBorder.createBlackLineBorder());
        vv.addMouseListener(myadapter);
        vv.addMouseMotionListener(myadapter);
        vv.getRenderContext().setVertexFillPaintTransformer(new MyVertexFillPaintTransformer(graph));
        vv.getRenderContext().setVertexLabelTransformer(new ToStringLabeller<SequenceVertex>());
        
        return vv;
	}
	
	@Override
	protected MyActionlistener<SequenceVertex,String> initActionListener(MyVisualizationServer<SequenceVertex,String> vv, DataPanel<String> dataPanel){
		return new ComparatorActionListener(vv.getGraphLayout(),dataPanel);
	}
	
	public static void main(String[] args){
		
		ComparatorMain myframe = new ComparatorMain();
		myframe.setVisible(true);
		
	}
}
